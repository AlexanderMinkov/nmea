/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nmeaparser;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author aamin
 */
public class NMEAManipulatorTest {
    
    @Test
    public void testCheck() {
        System.out.println("checkCRC");
        String in1 = "$GPRMC,114032.106,A,5231.368,N,01323.834,E,2005.0,155.3,170320,000.0,W*4E";
        String in2 = "$GPRMC,114034.106,A,5231.663,N,01325.899,E,2137.7,274.3,170320,000.0,W*40";
        try {
            Scanner sc = new Scanner(Paths.get("C:\\Program Files\\NMEA\\output.nmea"));
            NMEAReader instance = new NMEAReader(sc);
            NMEAManipulator man = new NMEAManipulator(instance.readFile());
             assertEquals("4E", man.checkCRC(in1));
             assertEquals("40", man.checkCRC(in2));
             assertEquals(Float.parseFloat("52.5228"), Float.parseFloat(man.convertLatToDec("5231.368")));
             assertEquals(Float.parseFloat("13.397233"), Float.parseFloat(man.convertLongToDec("01323.834")));
             
        // TODO review the generated test code and remove the default call to fail.
        
        } catch (IOException ex) {
            Logger.getLogger(NMEAManipulatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
    }
    
}
