/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nmeaparser;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aamin
 */
public class main {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(Paths.get("C:\\Program Files\\NMEA\\output.nmea"))) {
            NMEAReader nm = new NMEAReader(sc);
            NMEAManipulator manipulator = new NMEAManipulator(nm.readFile());
            manipulator.getInfo();
            manipulator.print();
        } catch (Exception ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
