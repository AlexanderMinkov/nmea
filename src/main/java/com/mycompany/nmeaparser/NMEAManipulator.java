 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nmeaparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aamin
 */
public class NMEAManipulator {
    private ArrayList<String> list = new ArrayList<>();
    private ArrayList<String> result = new ArrayList<>();

    public NMEAManipulator(ArrayList<String> list) {
        this.list = list;
    }
    
    public String convertTime(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat("hhmmss");
        try {  
            Date date1 = formatter.parse(time);
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");  
            String strDate = dateFormat.format(date1);
            return strDate;
        } catch (ParseException ex) {
            Logger.getLogger(NMEAManipulator.class.getName()).log(Level.SEVERE, null, ex);
            time = "";
        }
        return time;
    }
    
    public String convertLatToDec(String cordinate) {
        try{
            int grad = Integer.parseInt(cordinate.substring(0,2));
            float min = Float.parseFloat(cordinate.substring(2));
            if(min >= 0) {
             return String.valueOf((min/60.0) + grad);
            }
            return String.valueOf(-(min/60.0) + grad);
        }
        catch(NumberFormatException e) {
            Logger.getLogger(NMEAManipulator.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
       
    }
    public String convertLongToDec(String cordinate) {
        try{
            int grad = Integer.parseInt(cordinate.substring(0,3));
            float min = Float.parseFloat(cordinate.substring(3));
            if(min >= 0) {
                return String.valueOf((min/60.0) + grad);
            }
            return String.valueOf(-(min/60.0) + grad);
        }
        catch(NumberFormatException e) {
            Logger.getLogger(NMEAManipulator.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
       
    }
    public String checkCRC(String in) {
        if(in.length() == 0) {
            return "";
        }
        int checksum = 0;
        if (in.startsWith("$")) {
            in = in.substring(1, in.length());
        }

        int end = in.indexOf('*');
        if (end == -1) {
            end = in.length();
        }
        for (int i = 0; i < end; i++) {
            checksum = checksum ^ in.charAt(i);
        }
        String hex = Integer.toHexString(checksum);
        if (hex.length() == 1) {
            hex = "0" + hex;
        }
        return hex.toUpperCase();
    }

    public void getInfo() {
        for (String i : list) {
            String[] arr = i.split(",");
            if ("$GPRMC".equals(arr[0])) {
                result.add(arr[0] + " " + convertTime(arr[1]) + " " + convertLatToDec(arr[3]) + " " + arr[4] + " " + convertLongToDec(arr[5]) + " " + arr[6] + " " + arr[7]);
            } else {
                result.add(arr[0] + " " + convertTime(arr[1]) + " " + convertLatToDec(arr[2]) + " " + arr[3] + " " + convertLongToDec(arr[4]) + " " + arr[5]);
            }
        }
    }
    public void print() {
        System.out.println("RES:");
        for(String i : result) {
            System.out.println(i);
        }
    }

     
}
