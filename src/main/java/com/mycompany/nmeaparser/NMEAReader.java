/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nmeaparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author aamin
 */
public class NMEAReader {
    private final Scanner sc;
    public NMEAReader(Scanner sc) {
        this.sc = sc;
    }
    public ArrayList<String> readFile() {
        ArrayList<String> list = new ArrayList<>();
        while(sc.hasNextLine()) {
            String str = sc.nextLine();
            if(str.startsWith("$GPRMC") || str.startsWith("$GPGGA")) {
                list.add(str);
            }
        }
        return list;
    }
}
